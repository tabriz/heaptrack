Introduction
===============
This is the heaptrack (a heap memory profiler for Linux) packaged inside a container which can run with `Charliecloud <https://github.com/hpc/charliecloud/>`_ by users on MPCDF clusters. To access the original heaptrack code and its documentations please visit the `KDE GitLab <https://invent.kde.org/sdk/heaptrack>`_.

Basic usage
===============
1. Install Charliecloud.

You can use the available Charliecloud module

.. code-block:: shell

 module load charliecloud

or if you need a more recent version, you can compile the Charliecloud from source

.. code-block:: shell

 wget https://github.com/hpc/charliecloud/releases/download/v0.30/charliecloud-0.30.tar.gz
 tar -xf charliecloud-0.30.tar.gz
 cd charliecloud-0.30
 ./autogen.sh
 ./configure
 make

2. Pull the heaptrack container from registry and extract its content to a directory.

.. code-block:: shell

 ch-image pull gitlab-registry.mpcdf.mpg.de/tabriz/heaptrack/master:latest
 ch-convert gitlab-registry.mpcdf.mpg.de/tabriz/heaptrack/master:latest ./heaptrack

3. Run your code inside the container to gather profiling data

.. code-block:: shell

 ./ch-run -w -b /home/$USER:/mnt ./heaptrack -- heaptrack /mnt/your_exec

`-w` makes the container writable, `-b /home/$USER:/mnt` mounts the user folder to the /mnt inside the container. All the commands after `--` are executed inside the container.
The code will exit with the path to your profile data file

4. Run the graphical analyzer on the gathered data

.. code-block:: shell

 ./ch-run ./heaptrack -- heaptrack_gui heaptrack.your_exec.pid.zst

You need to have the X11 forwarding enabled to run this from a secure shell session. 
Alternatively, you can use `heaptrack_print` instead of `heaptrack_gui` to have a text-based summary of the gathered profiling data.

Accessing the linked libraries from inside of the container
=============================================================
You must first create mount points for the paths to your libraries inside the container to use them. For example on the Cobra cluster:

.. code-block:: shell

 mkdir heaptrack/mpcdf
 mkdir heaptrack/cobra
 ./ch-run -w -b /u/$USER:/mnt -b /mpcdf:/mpcdf -b /cobra:/cobra ./heaptrack -- heaptrack /mnt/your_exec


Profiling MPI applications
==========================
You can run the container with an MPI launcher to profile your application while running in parallel. Heaptrack will create one output file for each pid (rank).

.. code-block:: shell

 mpirun -n 2 ./ch-run -w -b /u/$USER:/mnt -b /mpcdf:/mpcdf -b /cobra:/cobra ./heaptrack -- heaptrack /mnt/your_exec

To run the application with slurm you have to mount the path to the PMI virtualization library inside the container and set the environment variable for using it.

.. code-block:: shell

 mkdir heaptrack/cobra_lib64
 export I_MPI_PMI_LIBRARY=/cobra_lib64/libpmi2.so.0
 srun -n 2 ./ch-run -w -b /u/$USER:/mnt -b /mpcdf:/mpcdf -b /cobra:/cobra -b /usr/lib64:/cobra_lib64 ./heaptrack -- heaptrack /mnt/your_exec

If you want to run the heaptrack in a specific directory, you can use `--cd` flag of Charliecloud.

.. code-block:: shell

 srun -n 2 ./ch-run -w -b /u/$USER:/mnt -b /mpcdf:/mpcdf -b /cobra:/cobra --cd=/mnt/path/inside/container ./heaptrack -- heaptrack ./your_exec

External links
=================
* `Charliecloud documentation <https://hpc.github.io/charliecloud/>`_
* `Heaptrack official repo <https://github.com/KDE/heaptrack>`_
